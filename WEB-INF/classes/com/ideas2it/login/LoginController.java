package com.ideas2it.login;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;  
import org.springframework.web.bind.annotation.ModelAttribute;  
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;  
import org.springframework.web.servlet.ModelAndView; 

/**
 * Servlet implementation class LoginServlet
 */
@Controller
public class LoginController extends HttpServlet {
    private final String user = "admin";
    private final String pass = "123456";

    @RequestMapping(value = "jsp/login" ,method = RequestMethod.POST)
    public ModelAndView login(HttpServletRequest request) {
        String userName = request.getParameter("userName");
        String password = request.getParameter("password");  
        ModelAndView modelAndView = null;    
        if (user.equals(userName) && pass.equals(password)) {
            HttpSession session = request.getSession(true);
            session.setAttribute("user","keerthi");
            modelAndView = new ModelAndView("redirect:./index.jsp");
        } else {
            modelAndView = new ModelAndView("login");
            modelAndView.addObject("message", "\t\tInvalid UserName Or Password!!!\t\t");
        }
        return modelAndView;
    }

}

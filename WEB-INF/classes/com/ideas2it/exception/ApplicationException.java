package com.ideas2it.exception;

/**
 * <p>
 * Provides Custom Exception for RunTime Exceptions. 
 *
 * </p>
 */
public class ApplicationException extends Exception {
    
    public ApplicationException(String reason) {
        super(reason);
    }

    public ApplicationException(Throwable cause) {
        super(cause);
    }

    public ApplicationException(String reason, Throwable cause) {
        super(reason, cause);
    }

}

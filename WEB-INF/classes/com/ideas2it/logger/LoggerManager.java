package com.ideas2it.logger;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * <p>
 * Provides Logger and Handle Logging for Exceptions.
 *
 * </p>
 */
public class LoggerManager {

    private static Logger logger;
    private static final String URL = "logger/Log4j.xml";

    public LoggerManager(String className) {
        logger = Logger.getLogger(className);
    }

   /**
    * <p>
    * Provides logger config for Connection 
    * 
    * </p>
    */
    public static void getLoggerConfig() {
        DOMConfigurator.configure(URL);
    }

   /**
    * <p>
    * Provides Debug logger for Logging  
    * 
    * </p>
    */  
    public static void logDebug(String cause, Exception e) {
        getLoggerConfig();
        logger.log(Level.DEBUG, cause);
    }

   /**
    * <p>
    * Provides Error logger for Logging 
    * 
    * </p>
    */
    public static void logError(String cause, Exception e) {
        getLoggerConfig();
        logger.log(Level.ERROR, cause);
    }

   /**
    * <p>
    * Provides info logger for Logging  
    * 
    * </p>
    */    
    public static void logInfo(String cause) {
        getLoggerConfig();
        logger.log(Level.INFO, cause);
    }

   /**
    * <p>
    * Provides Warning logger for Logging 
    * 
    * </p>
    */
    public static void logWarning(String cause, Exception e) {
        getLoggerConfig();
        logger.log(Level.WARN, cause);
    }

}

package com.ideas2it.common;

/**
 * <p>
 * Provides Constant values for primitive data Types.
 *
 * </p>
 */
public class Constants {
    
    public static final int NO_0 = 0;
    public static final int NO_1 = 1;
    public static final int NO_18 = 18;
    public static final int NO_2 = 2;
    public static final int NO_3 = 3;
    public static final int NO_4 = 4;
    public static final int NO_5 = 5;
    public static final int NO_6 = 6;
    public static final int NO_7 = 7;
    public static final int NO_8 = 8;    
    public static final String ISACTIVE = "isActive";
    public static final String ADDITION = "\n1.Addition";
    public static final String ADDRESSES = "\nList Of Addresses:";
    public static final String ADDRESS_MENU = "Add Address = '1'! Delete ='2'";
    public static final String ADDRESS = "\nEnter Address Details:";
    public static final String ADDRESS_NO = "\nAddress No:";
    public static final String APP_EXCEPTION = "Something Wrong!!Try Again later";
    public static final String ASSIGN_EMP = "\n5.Assign Employee to Project";
    public static final String ASSIGN_PROJECT = "\n5.Assign Client to Project";
    public static final String CLIENT_ID = "\nClient Id:";
    public static final String CLIENT_MANAGEMENT ="\n\t\t3.Client - Management";
    public static final String COMPLETION = "Process Completed Successfully!!!";
    public static final String CONTINUE_MENU ="\n1.Continue\n0.Exit";
    public static final String CRITERIA ="Emp Age must be Greater than 18";
    public static final String DATE_EXCEPTION = "Invalid Format!!! Try Again";
    public static final String DATE_FORMAT = "DD/MM/YYYY";;
    public static final String DATE = "\nEnter Valid Date(DD/MM/YYYY):";
    public static final String DEASSIGN_EMP = "\n6.De-Assign Employee-Project";
    public static final String DEASSIGN_PROJECT = "\n6.De-Assign Client-Project";
    public static final String DELETION = "\n2.Deletion";
    public static final String DESCRIPTION = "\nDescription(MIN 150 CHAR ):";
    public static final String DISPLAY = "\n3.Display Details";
    public static final String DISTRICT = "\nDistrict :";
    public static final String DOB = "\nDate Of Birth(DD/MM/YYYY):";
    public static final String DOJ = "\nDate Of Joining(DD/MM/YYYY):";
    public static final String DOMAIN = "\nDomain:";
    public static final String DOOR_NO = "\nDoorNo :";
    public static final String EMPLOYEE_ID = "\nEmployee Id:";
    public static final String EMPLOYEE_MANAGEMENT ="\n\t\t1.Employee-Management";
    public static final String ERROR_ADD = "Error Occured while Adding";
    public static final String ERROR_ASSIGN  = "Error Occured while Assign";
    public static final String ERROR_DEASSIGN = "Error Occured while De-Assign";
    public static final String ERROR_DELETE = "Error Occured while Deleting";
    public static final String ERROR_RETRIVE = "Error Occured while Retriving";
    public static final String ERROR_UPDATE = "Error Occured while Updating";
    public static final String COMPLETE_ADD = "Successfully!!!Completed Adding";
    public static final String HYPHEN ="\t---------------------------------";
    public static final String ID = "id";
    public static final String INVALID_ENTRY = "Invalid Entry";
    public static final String IO_EXCEPTION = "Invalid Data Entered!!Try Again";
    public static final String MAILID = "\nMailID:";
    public static final String MANAGEMENT ="\t\tCompany - Management";
    public static final String NAME = "\nName:";
    public static final String PROJECT_ID = "\nProject Id:";
    public static final String PROJECT = "\nProject:";
    public static final String PROJECT_MANAGEMENT ="\n\t\t2.Project-Management";
    public static final String STATE = "\nState :";
    public static final String STREET = "\nStreet :";
    public static final String STRING_C = "C"; 
    public static final String STRING_E = "E"; 
    public static final String TOWN = "\nTown :";
    public static final String UPDATE = "\n4.Update Details";
    public static final String ADDRESS_UPDATION = (new StringBuilder("Do you")
        .append(" wish to update Address Press '1'").toString());
    public static final String CONFIRMATION = (new StringBuilder("Are you")
        .append(" Sure !!! You wish to Delete !!!press '1'").toString());

}

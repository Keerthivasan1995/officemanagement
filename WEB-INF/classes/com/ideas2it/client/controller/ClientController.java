package com.ideas2it.client.controller;

import java.util.ArrayList;
import java.util.List;
 
import javax.servlet.http.HttpServlet;
import org.springframework.stereotype.Controller;  
import org.springframework.web.bind.annotation.ModelAttribute;  
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;  
import org.springframework.web.servlet.ModelAndView;  

import com.ideas2it.address.model.Address;
import com.ideas2it.client.model.Client;
import com.ideas2it.client.service.ClientService;
import com.ideas2it.client.service.impl.ClientServiceImpl;
import com.ideas2it.common.Constants;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.project.model.Project;
import com.ideas2it.project.service.impl.ProjectServiceImpl;
import com.ideas2it.project.service.ProjectService;

/**
 * <p>
 * Provides client updation like addition,deletion,
 * update and display of client details.
 *
 * </p>
 * @version          08/09/2017
 * @author           Keerthi vasan.V
 *
 */
@Controller
public class ClientController extends HttpServlet {   
    private ClientService clientService;
    private ProjectService projectService;

    public void setProjectService(ProjectService projectService) {
        this.projectService = projectService;
    }

    public void setClientService(ClientService clientService) {
        this.clientService = clientService;
    }

    /**
     * <p>
     * Provides for addition of client Details.
     *
     * </p>
     */
    @RequestMapping(value = "jsp/client_addition", method = RequestMethod.POST)
    public ModelAndView addClient(@ModelAttribute("client") Client client, 
            @ModelAttribute("address") Address address) {
        List<Address> addresses = new ArrayList<>();
        ModelAndView modelAndView = null;
        List<Client> clients;
        try {
            addresses.add(address);
            client.setAddresses(addresses);
            clientService.addClient(client);
            clients = clientService.retriveClients();
            modelAndView = new ModelAndView("clients_display");
            modelAndView.addObject("clients", clients);
            modelAndView.addObject("message","\t\tClient Details Added Successfully!!!\t\t");
        }
        catch (ApplicationException e) {
            modelAndView = new ModelAndView("5xx");
        }
        return modelAndView;
    }  

    /**
     * <p>
     * Gets an input from user and passes a value  
     * for deletion.
     *
     * </p>
     */
    @RequestMapping(value = "jsp/client_deletion", method = RequestMethod.GET)
    public ModelAndView deleteClient(@RequestParam("id") int id) {
        ModelAndView modelAndView = null;
        boolean state;
        List<Client> clients;
        try {
            deassignClientToProjects(id);
            state = clientService.removeClient(id);
            clients = clientService.retriveClients();
            modelAndView = new ModelAndView("clients_display");
            modelAndView.addObject("clients", clients);
            if (state) {
                modelAndView.addObject("message", "\t\tClient Deleted Successfully\t\t");
            } else {
                modelAndView.addObject("message", "\t\tEntered Client Id Not Exist\t\t");
            }
        }
        catch (ApplicationException e) {
            modelAndView = new ModelAndView("5xx");
        }
        return modelAndView;   
    }

    /**
     * <p>
     * Displays List of Client 
     *
     * </p>
     */
    @RequestMapping(value = "jsp/clients_display", method = RequestMethod.GET)
    public ModelAndView displayClients() {
        ModelAndView modelAndView = null;        
        List<Client> clients;
        try {
            clients = clientService.retriveClients();
            modelAndView = new ModelAndView("clients_display");
            modelAndView.addObject("clients", clients);
        }
        catch (ApplicationException e) {
            modelAndView = new ModelAndView("5xx");
        }
        return modelAndView;
    }  
  
    /**
     * <p>
     * Provides updation of client 
     *
     * </p>
     */
    @RequestMapping(value = "jsp/client_updation",
    params="operation=update", method = RequestMethod.GET)
    public ModelAndView updateClient(@ModelAttribute("client") Client client, 
            @ModelAttribute("address") Address address) {
        List<Address> addresses = new ArrayList<>();
        ModelAndView modelAndView = null;
        List<Client> clients;
        try {
            addresses.add(address);
            client.setAddresses(addresses);
            clientService.modifyClient(client);
            if (client.getProjects() != null) {            
                deassignClientToProjects(client, client.getProjectId());
            }
            clients = clientService.retriveClients();
            modelAndView = new ModelAndView("clients_display");
            modelAndView.addObject("clients", clients);
            modelAndView.addObject("message", "\t\tClient Details Updated Successfully\t\t");
        }
        catch (ApplicationException e) {
            modelAndView = new ModelAndView("5xx");
        }
        return modelAndView;
    }

    /**
     * <p>
     * Provides Client Details based on id 
     *
     * </p>
     */  
    @RequestMapping(value = "jsp/client_details", method = RequestMethod.GET)
    public ModelAndView getClientDetails(@RequestParam("id") int id) {
        ModelAndView modelAndView = null;        
        Client client;     
        try {
            client = clientService.getClientById(id);          
            modelAndView = new ModelAndView("client_details");
            modelAndView.addObject("client", client);  
        }
        catch (ApplicationException e) {
            modelAndView = new ModelAndView("5xx");
        } 
        return modelAndView;   
    }  

    /**
     * <p>
     * Provides Addition of Project-client Association Details
     *
     * </p>
     */
    @RequestMapping(value = "jsp/client_project_assign",
    params="operation=assignProjects",method = RequestMethod.GET) 
    public ModelAndView assignClientToProjects(@RequestParam("id") int id,
            @RequestParam("projectId") int projectId) {
       ModelAndView modelAndView = null;
        List<Project> projects = new ArrayList<>();
        Project project;
        Client client;
        try {
            client = clientService.getClientById(id);
            project = projectService.getProjectById(projectId);
            projectService.addClientToProject(project, client);
            projects.add(project);
            clientService.addProjectToClient(projects, client);
            modelAndView = new ModelAndView("redirect:client_details");
            modelAndView.addObject("id", id);
        }
        catch (ApplicationException e) {
            modelAndView = new ModelAndView("5xx");
        }
        return modelAndView;
    }

    /**
     * <p>
     * Provides Deletion of Project-client Association Details
     *
     * </p>
     */
    public void deassignClientToProjects(Client client, int projectId) 
    throws ApplicationException {
        Project project;
        try {
            project = projectService.getProjectById(projectId);
            clientService.deassignProjectFromClient(client, projectId, project);
            projectService.removeClientToProject(project);
        }
        catch (ApplicationException e) {
            throw (e);
        }
    }

    /**
     * <p>
     * Provides Client Details based on id 
     *
     * </p>
     */    
    @RequestMapping(value = "jsp/client_updation",
    params="operation=getClientById", method = RequestMethod.GET)
    public ModelAndView getClientById(@RequestParam("id") int id) {
        ModelAndView modelAndView = null;      
        Client client;
        List<Client> clients;     
        try {
            client = clientService.getClientById(id);           
            if (client != null) {
                modelAndView = new ModelAndView("client_updation");
                modelAndView.addObject("client", client);
                modelAndView.addObject("message","message");
            } else {
            clients = clientService.retriveClients();
            modelAndView = new ModelAndView("clients_display");
            modelAndView.addObject("clients", clients);  
            modelAndView.addObject("message", "\t\tEntered Client Id Not Exist\t\t"); 
            } 
        }
        catch (ApplicationException e) {
            modelAndView = new ModelAndView("5xx");
        }
        return modelAndView;    
    }

    /**
     * <p>
     * Gets an input from user and passes a value  
     * for display of list.
     *
     * </p>
     */
    @RequestMapping(value = "jsp/client_project_assign",
    params="operation=getProjects",method = RequestMethod.GET) 
    public ModelAndView getProjects(@RequestParam("id") int id) {
        ModelAndView modelAndView = null;
        Client client;
        List<Project> projects;
        try {
            client = clientService.getClientById(id);
            projects = projectService.retriveProjects();
            modelAndView = new ModelAndView("client_project_assign");
            modelAndView.addObject("client", client);
            modelAndView.addObject("projects", projects);
        }
        catch (ApplicationException e) {
            modelAndView = new ModelAndView("5xx");
        }
        return modelAndView;
    }  

    /**
     * <p>
     * Provides Deletion of Project-client Association Details
     *
     * </p>
     */
    public void deassignClientToProjects(int clientId) 
    throws ApplicationException {
        int projectId;
        List<Project> projects = new ArrayList<>();
        Client client;
        try {
            client = clientService.getClientById(clientId);
            projects = client.getProjects();
            for (Project projecttemp : projects) {
                projectId = projecttemp.getId();
                clientService.deassignProjectFromClient(client, 
                    projectId, projecttemp);
                projectService.removeClientToProject(projecttemp);
            }
        }
        catch (ApplicationException e) {
            throw (e);
        }
    }

}

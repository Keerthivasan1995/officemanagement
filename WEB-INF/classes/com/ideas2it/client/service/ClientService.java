package com.ideas2it.client.service;

import java.util.List;

import com.ideas2it.address.model.Address;
import com.ideas2it.client.model.Client;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.project.model.Project;

/**
 * <p>
 * Provides control of an addition , deletion , update ,
 * and Display the details of client.
 * </p>
 */
public interface ClientService {
    
   /**
    * <p>
    * Gets an input and passes value 
    * and add in array list.
    *
    * </p>
    * @param      client
    *             Object of client is passed
    */  
    void addClient(Client client) throws ApplicationException;

    /**
     * <p>
     * Deletes client details by Client ID 
     *
     * </p>
     * @param         client ID is validated
     *
     */
    boolean removeClient(int targetId) throws ApplicationException;

    /**
     * <p>
     * Provide details of all client
     * 
     * </p>
     * @return     clients
     *             Client details is returned as list
     */ 
    List<Client> retriveClients() throws ApplicationException;

    /**
     * <p>
     * Updates the particular field in the
     *  list  using loop and conditions.
     *
     * </p> 
     * @param      object
     *             object of client is passed and updated
     *                 
     */
    void modifyClient(Client client) throws ApplicationException;

    /**
     * <p>
     * Provides an client object based on client Id
     * 
     * </p>
     * @param        Client Id
     *               Id of client is passed
     * @return       Client object is returned
     */
    Client getClientById(int clientId) throws ApplicationException;

    /**
     * <p>
     * Provides an Addition of Project to client
     * 
     * </p>
     * @param        Client 
     *               Object of client is passed
     * @param        projects
     *               Project List is passed
     */
    void addProjectToClient(List<Project> projects, Client client)
            throws ApplicationException;

    /**
     * <p>
     * Provides an Un assign of Project to client
     * 
     * </p>
     * @param        Client 
     *               Object of client is passed
     * @param        project
     *               Project object is passed
     */
    void deassignProjectFromClient(Client client, int projectId,
            Project project) throws ApplicationException;
}

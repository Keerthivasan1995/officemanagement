package com.ideas2it.client.service.impl;

import java.util.List;

import com.ideas2it.address.model.Address;
import com.ideas2it.client.dao.ClientDao;
import com.ideas2it.client.dao.impl.ClientDaoImpl;
import com.ideas2it.client.model.Client;
import com.ideas2it.client.service.ClientService;
import com.ideas2it.common.Constants;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.project.model.Project;

/**
 * <p>
 * Clientserviceimplement class implements Clientservice Interface and 
 * serves CRUD operation (create/read//updatedelete) operation
 * </p>
 */
public class ClientServiceImpl implements ClientService { 
    private ClientDao clientDao;

    public void setClientDao(ClientDao clientDao) {
        this.clientDao = clientDao;
    }
    /**
     * @see    com.ideas2it.client.service.ClientService;
     * @method addClient
     */
    public void addClient(Client client) throws ApplicationException {
        client.setIsActive(Boolean.TRUE);
        clientDao.insertClient(client);
    }

    /**
     * @see    com.ideas2it.client.service.ClientService;
     * @method removeClient
     */
    public boolean removeClient(int targetId) throws ApplicationException {
        Client client;      
        client = clientDao.getClientById(targetId);
        if (null != client) {
            client.setIsActive(Boolean.FALSE);
            clientDao.deleteClient(client);
            return Boolean.TRUE;
        }
        return Boolean.FALSE; 
    }

    /**
     * @see    com.ideas2it.client.service.ClientService;
     * @method retriveClient
     */
    public List<Client> retriveClients() throws ApplicationException {
        return clientDao.getClients();
    }

    /**
     * @see    com.ideas2it.client.service.ClientService;
     * @method modifyClient
     */
    public void modifyClient(Client client) throws ApplicationException {
        client.setIsActive(Boolean.TRUE);        
        clientDao.updateClient(client);
    }

    /**
     * @see    com.ideas2it.client.service.ClientService;
     * @method getClientById
     */
    public Client getClientById(int clientId) throws ApplicationException {
        Client client = null;
        client = clientDao.getClientById(clientId);
        if (null != client) {
            return client; 
        }
        return null;
    } 

    /**
     * @see    com.ideas2it.client.service.ClientService;
     * @Method addProjectToClient
     */ 
    public void addProjectToClient(List<Project> projects, Client client)
        throws ApplicationException {        
        client.setProjects(projects);
        clientDao.updateClient(client);
    }


    /**
     * @see     com.ideas2it.client.service.ClientService
     * @method  deassignProjectFromClient
     */
    public void deassignProjectFromClient(Client client, int projectId,
            Project project) throws ApplicationException {
        Object temp = null;
        List<Project> projects;
        if ((null != project) && (null != client)) {
            projects = client.getProjects();
            System.out.println(projects);
            for (Project projecttemp : projects) {
                if (projecttemp.getId() == projectId) {
                    temp = projecttemp;
                } 
            }
            projects.remove(temp);
            client.setProjects(projects);
            System.out.println(client.getProjects());
            clientDao.updateClient(client);
        }            
    }
}

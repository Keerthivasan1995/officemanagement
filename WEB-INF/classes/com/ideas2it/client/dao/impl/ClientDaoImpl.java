package com.ideas2it.client.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.HibernateException; 
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.ideas2it.address.model.Address;
import com.ideas2it.client.dao.ClientDao;
import com.ideas2it.client.model.Client;
import com.ideas2it.common.Constants;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.logger.LoggerManager;

/**
 * <p>
 * ClientDaoimplement class implements ClientDao Interface and 
 * serves CRUD operation (create/read/update/delete) operation
 * </p>
 */
public class ClientDaoImpl implements ClientDao {

    private static final LoggerManager loggerManager
                    = new LoggerManager(ClientDaoImpl.class.getName());

    private HibernateTemplate hibernateTemplate;
    
    public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
        this.hibernateTemplate = hibernateTemplate;
    }

    /**
     * @see    com.ideas2it.client.dao.ClientDao;
     * @method insertClient
     */
    public void insertClient(Client client) throws ApplicationException {
        try {
            hibernateTemplate.save(client);   
        } 
        catch (HibernateException e) {
            String msg = (new StringBuilder(Constants.ERROR_ADD)
                .append(Constants.NAME).toString());
            LoggerManager.logError(msg, e);
            throw new ApplicationException(msg);
        }
    }

    /**
     * @see    com.ideas2it.client.dao.ClientDao;
     * @method deleteClient
     */
    public boolean deleteClient(Client client) throws ApplicationException {
        try {
            hibernateTemplate.update(client);  
            return Boolean.TRUE; 
        } 
        catch (HibernateException e) {
            String msg = (new StringBuilder(Constants.ERROR_DELETE)
                .append(Constants.CLIENT_ID).append(client.getId()).toString());
            LoggerManager.logError(msg, e);
            throw new ApplicationException(msg);
        }
    }

    /**
     * @see    com.ideas2it.client.dao.ClientDao;
     * @method updateClient
     */
    public void updateClient(Client client) throws ApplicationException {
        try { 
            hibernateTemplate.update(client);
        }
        catch (HibernateException e) {
            String msg = (new StringBuilder(Constants.ERROR_UPDATE)
                .append(Constants.CLIENT_ID).append(client.getId()).toString());
            LoggerManager.logError(msg, e);
            throw new ApplicationException(msg);
        }
    }

    /**
     * @see    com.ideas2it.client.dao.ClientDao;
     * @method gettClient
     */
    public List<Client> getClients() throws ApplicationException {
        List<Client> clients;
        DetachedCriteria detachedCriteria;
        try {
            detachedCriteria = DetachedCriteria.forClass(Client.class);
            detachedCriteria.add(Restrictions.eq(Constants.ISACTIVE, Boolean.TRUE));
            clients= (List<Client>) hibernateTemplate
            .findByCriteria(detachedCriteria);
        }
        catch (HibernateException e) {
            LoggerManager.logError(Constants.ERROR_RETRIVE, e);
            throw new ApplicationException(Constants.ERROR_RETRIVE);
        }
        return clients;   
    }

    /**
     * @see    com.ideas2it.client.dao.ClientDao;
     * @method getClientById
     */
    public Client getClientById(int targetId) throws ApplicationException {
        List<Client> clients;
        Client client = null;
        DetachedCriteria detachedCriteria;
        try {
            detachedCriteria = DetachedCriteria.forClass(Client.class);
            detachedCriteria.add(Restrictions.eq(Constants.ISACTIVE, Boolean.TRUE))
                .add(Restrictions.eq(Constants.ID, targetId));
            clients = (List<Client>) hibernateTemplate
            .findByCriteria(detachedCriteria);
            if(!clients.isEmpty()) {
                client = clients.get(0);
            }
            return client;
        }
        catch (HibernateException e) {
            String msg = (new StringBuilder(Constants.ERROR_RETRIVE)
                .append(Constants.CLIENT_ID).append(targetId).toString());
            LoggerManager.logError(msg, e);
            throw new ApplicationException(msg);
        }
    }

}

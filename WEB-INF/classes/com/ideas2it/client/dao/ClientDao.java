package com.ideas2it.client.dao;

import java.util.List;

import com.ideas2it.client.model.Client;
import com.ideas2it.exception.ApplicationException;

/**
 * <p>
 * Provides insertion, update, deletion, retrival of Client details 
 * in database 
 * </p>
 */  
public interface ClientDao{

    /**
     * <p>
     * Provides insertion of Client Details
     * </p>
     * @param   client
     *          client is object of client details which is passed
     *          and inserted
     */
    void insertClient(Client client) throws ApplicationException;

    /**
     * <p>
     * Provides Deletion of client Details
     * </p>
     * @param   client
     *          client is object of client details which is passed
     * @return  TRUE If any client is deleted | FALSE if Id doesn't Exist 
     */
    boolean deleteClient(Client client) throws ApplicationException;

    /**
     * <p>
     * Provides updation of Client Details into Table
     * </p>
     * @param   client
     *          client is object of client details which is passed
     *          and updated
     */
    void updateClient(Client client) throws ApplicationException;

    /**
     * <p>
     * Provides Retrival of client Details
     * </p>
     * @return  resultSet
     *          which is list of Clients is returned
     */
    List<Client> getClients() throws ApplicationException;

    /**
     * <p>
     * Provides Retrival of Client Details
     * </p>
     * @param   targetId
     *          which is Client ID is passed
     * @return  resultSet
     *          which is object of Client is returned
     */
    Client getClientById(int targetId) throws ApplicationException;

}

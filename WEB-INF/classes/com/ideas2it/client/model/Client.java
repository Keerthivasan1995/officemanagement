package com.ideas2it.client.model;

import java.util.List;

import com.ideas2it.address.model.Address;
import com.ideas2it.common.Constants;
import com.ideas2it.project.model.Project;

/**
 * <p>
 * Gets an input and sets a value
 * 
 * </p>
 */
public class Client {
    private boolean isActive;
    private int id;
    private int projectId;
    private String name;
    private String mailId;
    private List<Address> addresses;
    private List<Project> projects;

    public int getId() {
        return this.id;
    }

    public void setId(int clientId) {
        this.id = clientId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String clientName) {
        this.name = clientName;
    }

    public String getMailId() {
        return this.mailId;
    }

    public void setMailId(String clientMailId) {
        this.mailId = clientMailId;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    public List<Address> getAddresses() {
        return this.addresses;
    }

    public List<Project> getProjects() {
        return this.projects;
    }

   public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public boolean getIsActive() {
        return this.isActive;
    }

    public int getProjectId() {
        return this.projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    /** 
     * <p>
     * Provides Clients Details as string by toString method.
     * </p>
     */
    public String  toString() {
        return (new StringBuilder(Constants.CLIENT_ID).append(this.id)
        .append(Constants.NAME).append(this.name).append(Constants.MAILID)
        .append(this.mailId).append(Constants.ADDRESSES)
        .append(this.addresses).toString());
    }

}



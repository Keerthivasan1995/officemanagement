package com.ideas2it.employee.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.HibernateException; 
import org.springframework.orm.hibernate3.HibernateTemplate;
 
import com.ideas2it.common.Constants;
import com.ideas2it.employee.dao.EmployeeDao;
import com.ideas2it.employee.model.Employee;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.logger.LoggerManager;
import com.ideas2it.project.model.Project;

/**
 * <p>
 * EmployeeDaoimplement class implements EmployeeDao Interface and 
 * serves CRUD operation (create/read/update/delete) operation
 * </p>
 */ 
public class EmployeeDaoImpl implements EmployeeDao {
    private static final LoggerManager loggerManager
                    = new LoggerManager(EmployeeDaoImpl.class.getName());                            
    private HibernateTemplate hibernateTemplate;
    
    public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
        this.hibernateTemplate = hibernateTemplate;
    }

    /**
     * @see    com.ideas2it.employee.dao.EmployeeDao;
     * @method insertEmployee
     */
    public void insertEmployee(Employee employee) throws ApplicationException {
        try {
            hibernateTemplate.save(employee); 
        } 
        catch (HibernateException e) {
            String msg = (new StringBuilder(Constants.ERROR_ADD)
            .append(Constants.NAME).append(employee.getName()).toString());
            loggerManager.logError(msg, e);
            e.printStackTrace();
            throw new ApplicationException(msg);
        }
    }

    /**
     * @see    com.ideas2it.employee.dao.EmployeeDao;
     * @method deleteEmployee
     */
    public boolean deleteEmployee(Employee employee)
            throws ApplicationException {
        try {
             hibernateTemplate.update(employee); 
            return Boolean.TRUE; 
        } 
        catch (HibernateException e) {
            String msg = (new StringBuilder(Constants.ERROR_DELETE)
            .append(Constants.EMPLOYEE_ID).append(employee.getId()).toString());
            loggerManager.logError(msg, e);
            throw new ApplicationException(msg);
        }
    }

    /**
     * @see    com.ideas2it.employee.dao.EmployeeDao;
     * @method updateEmployee
     */
    public void updateEmployee(Employee employee)
            throws ApplicationException {
        try {
             hibernateTemplate.update(employee);
        }
        catch (HibernateException e) {
            String msg = (new StringBuilder(Constants.ERROR_UPDATE)
            .append(Constants.EMPLOYEE_ID).append(employee.getId()).toString());
            loggerManager.logError(msg, e);
            throw new ApplicationException(msg);
        }     
    }

    /**
     * @see    com.ideas2it.employee.dao.EmployeeDao;
     * @method getEmployees
     */
    public List<Employee> getEmployees() throws ApplicationException {
        List<Employee> employees;
        DetachedCriteria detachedCriteria;
        try {
            detachedCriteria = DetachedCriteria.forClass(Employee.class);
            detachedCriteria.add(Restrictions.eq(Constants.ISACTIVE, Boolean.TRUE));
            employees =  (List<Employee>) hibernateTemplate
            .findByCriteria(detachedCriteria);
        }
        catch (HibernateException e) {
            loggerManager.logError(Constants.ERROR_RETRIVE, e);
            throw new ApplicationException(Constants.ERROR_RETRIVE);
        }
        return employees;
    }

    /**
     * @see    com.ideas2it.employee.dao.EmployeeDao;
     * @method getEmployeeById
     */
    public Employee getEmployeeById(int targetId) throws ApplicationException {
        Employee employee = null;
        List<Employee> employees;
        DetachedCriteria detachedCriteria;
        try {
            detachedCriteria = DetachedCriteria.forClass(Employee.class);
            detachedCriteria.add(Restrictions.eq(Constants.ISACTIVE, Boolean.TRUE))
                .add(Restrictions.eq(Constants.ID, targetId));
            employees =  (List<Employee>) hibernateTemplate
            .findByCriteria(detachedCriteria);
            if(!employees.isEmpty()) {
                employee = employees.get(0);
            }
            return employee;
        }
        catch (HibernateException e) {
            String msg = (new StringBuilder(Constants.ERROR_RETRIVE)
            .append(Constants.EMPLOYEE_ID).append(targetId).toString());
            loggerManager.logError(msg, e);
            throw new ApplicationException(msg);
        }
    }

}

package com.ideas2it.employee.dao;

import java.util.List;

import com.ideas2it.employee.model.Employee;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.project.model.Project;

/**
 * <p>
 * Provides insertion, updation, deletion, retrival of Employee details 
 * in database 
 * </p>
 */  
public interface EmployeeDao {

    /**
     * <p>
     * Provides insertion of Employee Details
     * </p>
     * @param   employee
     *          employee is object of employee details which is passed
     *          and inserted
     */
    void insertEmployee(Employee employee) throws ApplicationException;

    /**
     * <p>
     * Provides Deletion of employee Details
     * </p>
     * @param   employee
     *          Object of employee is passed
     * @return  TRUE If employee is deleted | FALSE if Id doesn't Exist 
     */
    boolean deleteEmployee(Employee employee) throws ApplicationException;
    /**
     * <p>
     * Provides updation of Employee Details
     * </p>
     * @param   employee
     *          employee is object of employee details which is passed
     *          and updated
     */
    void updateEmployee(Employee employee) throws ApplicationException;

    /**
     * <p>
     * Provides Retrival of employee Details
     * </p>
     * @return  resultSet
     *          which is list of Employees is returned
     */
    List<Employee> getEmployees() throws ApplicationException;

    /**
     * <p>
     * Provides Retrival of Employee Details
     * </p>
     * @param   targetId
     *          which is employee ID is passed
     * @return  resultSet
     *          which is object of Employee is returned
     */
    Employee getEmployeeById(int targetId) throws ApplicationException;

}

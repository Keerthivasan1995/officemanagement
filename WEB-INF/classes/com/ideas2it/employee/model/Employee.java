package com.ideas2it.employee.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.text.ParseException;

import com.ideas2it.address.model.Address;
import com.ideas2it.common.Constants;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.project.model.Project;

/**
 * <p>
 * Calculates an age and 
 * Gets an input and sets a value
 * 
 * </p>
 */
public class Employee {
    private boolean isActive;
    private int id;
    private String dob;
    private String doj; 
    private String mailId;
    private int projectId;
    private String name;
    private List<Address> addresses;
    private Project project;

    public void setId(int employeeId) {
        this.id = employeeId;
    }

    public int getId() {
        return this.id;
    }

    public void setName(String employeeName) {
        this.name = employeeName;
    }

    public String getName() {
        return this.name;
    }

    public void setMailId(String employeeMailId) {
        this.mailId = employeeMailId;
    }

    public String getMailId() {
        return this.mailId;
    }

    public void setDob(String employeeDob) {
        this.dob = employeeDob;
    }


    public String getDob() {
        return this.dob;
    }

    public void setDoj(String employeeDoj) {
        this.doj = employeeDoj;
    }

    public String getDoj() {
        return this.doj;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    public List<Address> getAddresses() {
        return this.addresses;
    }

    public Project getProject() {
        return this.project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public boolean getIsActive() {
        return this.isActive;
    }


    public int getProjectId() {
        return this.projectId;
    }

    public void setProjectId(int ProjectId) {
        this.projectId = projectId;
    }
    
    /** 
     * <p>
     * Provides Employee details as string by tostring method.
     * </p>
     */
    public String toString() {
        return (new StringBuilder(Constants.EMPLOYEE_ID).append(this.id)
        .append(Constants.NAME).append(this.name).append(Constants.MAILID)
        .append(this.mailId).append(Constants.DOB).append(this.dob)
        .append(Constants.DOJ).append(this.doj).append(Constants.ADDRESSES)
        .append(this.addresses).toString());
    }

}



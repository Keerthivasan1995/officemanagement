package com.ideas2it.employee.controller;

import java.util.ArrayList;
import java.util.List;
 
import javax.servlet.http.HttpServlet;
import org.springframework.stereotype.Controller;  
import org.springframework.web.bind.annotation.ModelAttribute;  
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;  
import org.springframework.web.servlet.ModelAndView;   

import com.ideas2it.address.model.Address;
import com.ideas2it.common.Constants;
import com.ideas2it.employee.model.Employee;
import com.ideas2it.employee.service.EmployeeService;
import com.ideas2it.employee.service.impl.EmployeeServiceImpl;
import com.ideas2it.exception.ApplicationException;

/**
 * <p>
 * Provides employee update like addition, deletion,
 * update and display of employee.
 *
 * </p>
 * @created          06/09/2017
 * @author           Keerthi vasan.V
 *
 */
@Controller
public class EmployeeController extends HttpServlet  {  
    private EmployeeService employeeService;

    public void setEmployeeService(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    /**
     * <p>
     * Provides addition of employee Details 
     *
     * </p>
     */
    @RequestMapping(value = "jsp/employee_addition", method = RequestMethod.POST) 
    public ModelAndView addEmployee(@ModelAttribute("employee") Employee employee, 
            @ModelAttribute("address") Address address) {
        List<Address> addresses = new ArrayList<>();
        ModelAndView modelAndView = null;
        List<Employee> employees;
        try {
            addresses.add(address);
            employee.setAddresses(addresses);
            employeeService.addEmployee(employee);
            employees = employeeService.retriveEmployees();
            modelAndView = new ModelAndView("employees_display");
            modelAndView.addObject("employees", employees);
            modelAndView.addObject("message", "\t\tEmployee Details Added Successfully!!!\t\t");
        }        
        catch (ApplicationException e) {
            modelAndView = new ModelAndView("5xx");    
        }
        return modelAndView;
    }

    /**
     * <p>
     * Provides deletion of Employee Details
     *
     * </p>
     */
    @RequestMapping(value = "jsp/employee_deletion", method = RequestMethod.GET) 
    public ModelAndView deleteEmployee(@RequestParam("id") int id){
        boolean state;
        ModelAndView modelAndView = null;
        List<Employee> employees;
        try {
            state = employeeService.removeEmployee(id);
            employees = employeeService.retriveEmployees();
            modelAndView = new ModelAndView("employees_display");
            modelAndView.addObject("employees", employees);
            if (state) {
                modelAndView.addObject("message", "\t\tEmployee Deleted Successfully!!!\t\t");
            } else {
                modelAndView.addObject("message", "\t\tEntered Employee Id Not Exist!!!!\t\t");
            }
        }
        catch (ApplicationException e) {
            modelAndView = new ModelAndView("5xx");
        }
        return modelAndView;  
    }

    /**
     * <p>
     * Display list of employees.
     *
     * </p>
     */
    @RequestMapping(value = "jsp/employees_display", method = RequestMethod.GET) 
    public ModelAndView displayEmployees() {
        ModelAndView modelAndView = null;
        List<Employee> employees = null;
        try {
            employees = employeeService.retriveEmployees();
            modelAndView = new ModelAndView("employees_display");
            modelAndView.addObject("employees", employees);
        }
        catch (ApplicationException e) {
            modelAndView = new ModelAndView("5xx");
        }
        return modelAndView;
    }  
  
    /**
     * <p>
     * update of employee details
     *
     * </p>
     */
   @RequestMapping(value = "jsp/employee_updation",
   params="operation=update", method = RequestMethod.GET)
    public ModelAndView updateEmployee(@ModelAttribute("employee") Employee employee, 
            @ModelAttribute("address") Address address) {
        ModelAndView modelAndView = null;
        List<Address> addresses = new ArrayList<>(); 
        List<Employee> employees;    
        try {
            addresses.add(address);
            employee.setAddresses(addresses);
            employeeService.modifyEmployee(employee);
                employees = employeeService.retriveEmployees();
                modelAndView = new ModelAndView("employees_display");
                modelAndView.addObject("employees", employees);
                modelAndView.addObject("message", "\t\tEmployee Details Updated SuccessFully!!!!\t\t");
        }        
        catch (ApplicationException e) {
            modelAndView = new ModelAndView("5xx");
        }
        return modelAndView;
    }

    /**
     * <p>
     * Provides Employee Details based on id 
     *
     * </p>
     */    
   @RequestMapping(value = "jsp/employee_updation",
   params="operation=getEmployeeById", method = RequestMethod.GET)
   public ModelAndView getEmployeeById(@RequestParam("id") int id) {
        ModelAndView modelAndView = null;        
        Employee employee; 
        List<Employee> employees;   
        try {
            employee = employeeService.getEmployeeById(id);           
            if (employee != null) {
                modelAndView = new ModelAndView("employee_updation");
                modelAndView.addObject("employee", employee);
            } else {
                employees = employeeService.retriveEmployees();
                modelAndView = new ModelAndView("employees_display");
                modelAndView.addObject("employees", employees);
                modelAndView.addObject("message", "\t\tEntered Employee Id Not Exist!!!!\t\t");
            }  
        }
        catch (ApplicationException e) {
            modelAndView = new ModelAndView("5xx");
        } 
        return modelAndView;   
    }

    /**
     * <p>
     * Provides Employee Details based on id 
     *
     * </p>
     */  
   @RequestMapping(value = "jsp/employee_details") 
   public ModelAndView getEmployeeDetails(@RequestParam("id") int id) {
        ModelAndView modelAndView = null;         
        Employee employee;     
        try {
            employee = employeeService.getEmployeeById(id);           
            modelAndView = new ModelAndView("employee_details");
            modelAndView.addObject("employee", employee);  
        }
        catch (ApplicationException e) {
            modelAndView = new ModelAndView("5xx");
        }
        return modelAndView;    
    }

}

package com.ideas2it.employee.service;

import java.util.List;

import com.ideas2it.address.model.Address;
import com.ideas2it.employee.model.Employee;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.project.model.Project;

/**
 * <p>
 * Provides control of an addition, deletion, update,
 * and Display the details of employee.
 * </p>
 */
public interface EmployeeService {                            

   /**
    * <p>
    * Gets an input and passes value 
    * and add in list.
    *
    * </p>
    * @param     employee
    *            Object or bean of employee is passed 
    */  
    void addEmployee(Employee employee) throws ApplicationException; 

    /**
     * <p>
     * Deletes employee details by employee ID 
     *
     * </p>
     * @param           targetId
     *                  Employee ID is validated and employee is deleted
     *
     */
    boolean removeEmployee(int targetId) throws ApplicationException;

    /**
     * <p>
     * Displays the list of employee 
     * 
     * </p>
     */ 
    List<Employee> retriveEmployees() throws ApplicationException;

    /**
     * <p>
     * Updates the employee details
     *
     * </p> 
     * @param     employee
     *            object of employee is passed and updated
     *
     *                 
     */
    void modifyEmployee(Employee employee) throws ApplicationException;    

    /**
     * <p>
     * Provides an Employee object based on Project Id
     * 
     *</p>
     * @return       Employee object is returned
     */ 
    Employee getEmployeeById(int employeeId) throws ApplicationException;

   /**
    * <p>
    * Provides association of project to Employees list.
    *
    * </p>
    * @param   employee and project 
    *          is passed and added to Employee project
    *
    */  
    void addProjectToEmployee(Employee employee, Project project)
            throws ApplicationException;

   /**
    * <p>
    * Provides Removal of project to Employees list.
    *
    * </p>
    * @param   employee  and project 
    *          is passed and project is  removed from Employee object
    *
    */  
    void removeProjectToEmployee(int employeeId)
            throws ApplicationException;

}

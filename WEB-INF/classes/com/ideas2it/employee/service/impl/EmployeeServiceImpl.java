package com.ideas2it.employee.service.impl;

import java.util.List;

import com.ideas2it.address.model.Address;
import com.ideas2it.common.Constants;
import com.ideas2it.employee.dao.EmployeeDao;
import com.ideas2it.employee.dao.impl.EmployeeDaoImpl;
import com.ideas2it.employee.model.Employee;
import com.ideas2it.employee.service.EmployeeService;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.project.model.Project;

/**
 * <p>
 * EmployeeServiceimplement class implements EmployeeService Interface and 
 * serves CRUD operation (create/read/update/delete) operation
 * </p>
 */
public class EmployeeServiceImpl implements EmployeeService { 
    private EmployeeDao employeeDao;                      

    public void setEmployeeDao(EmployeeDao employeeDao) {
        this.employeeDao = employeeDao;
    }

    /**
     * @see    com.ideas2it.employee.service.EmployeeService;
     * @method addEmployee
     */ 
    public void addEmployee(Employee employee) throws ApplicationException {
        employee.setIsActive(Boolean.TRUE);
        employeeDao.insertEmployee(employee);
    }

    /**
     * @see    com.ideas2it.employee.service.EmployeeService;
     * @method removeEmployee
     */ 
    public boolean removeEmployee(int targetId) throws ApplicationException {
        Employee employee = null;
        employee = employeeDao.getEmployeeById(targetId);
        if (null != employee) {
            employee.setIsActive(Boolean.FALSE);
            return employeeDao.deleteEmployee(employee);
        } else {
            return Boolean.FALSE;
        }
    }

    /**
     * @see    com.ideas2it.employee.service.EmployeeService;
     * @method retriveEmployees
     */ 
    public List<Employee> retriveEmployees() throws ApplicationException {  
        return employeeDao.getEmployees();
    }

    /**
     * @see    com.ideas2it.employee.service.EmployeeService;
     * @method modifyEmployee
     */ 
    public void modifyEmployee(Employee employee) throws ApplicationException {
        employee.setIsActive(Boolean.TRUE);
        employeeDao.updateEmployee(employee);
    }

    /**
     * @see    com.ideas2it.employee.service.EmployeeService;
     * @method getEmployeeById
     */ 
    public Employee getEmployeeById(int employeeId) 
            throws ApplicationException {
        Employee employee = null;
        employee = employeeDao.getEmployeeById(employeeId);
        if (null != employee) {                                
            return employee;
        }
        return null;
   } 

    /**
     * @see    com.ideas2it.employee.service.EmployeeService;
     * @method addProjectToEmployee
     */   
    public void addProjectToEmployee(Employee employee, Project project) 
            throws ApplicationException {        
        employee.setProject(project);
        employeeDao.updateEmployee(employee);
    }

    /**
     * @see    com.ideas2it.employee.service.EmployeeService;
     * @method removeProjectToEmployee
     */ 
    public void removeProjectToEmployee(int employeeId) 
            throws ApplicationException {        
        Employee employee = getEmployeeById(employeeId); 
        employee.setProject(null);
        employeeDao.updateEmployee(employee);
    }

}

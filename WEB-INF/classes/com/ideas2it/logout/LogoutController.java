package com.ideas2it.logout;
 
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;  
import org.springframework.web.bind.annotation.ModelAttribute;  
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;  
import org.springframework.web.servlet.ModelAndView;

/**
 * Servlet implementation class LogoutSession
 */
@Controller
public class LogoutController extends HttpServlet {

    @RequestMapping(value = "jsp/logout", method = RequestMethod.GET)
    public ModelAndView logout(HttpServletRequest request) {
        ModelAndView modelAndView = null;
        HttpSession session = request.getSession(false);
        session.removeAttribute("user");
        modelAndView = new ModelAndView("login");
        return modelAndView;    
    }
}

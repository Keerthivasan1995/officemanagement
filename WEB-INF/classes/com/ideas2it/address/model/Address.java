package com.ideas2it.address.model;

import com.ideas2it.client.model.Client;
import com.ideas2it.common.Constants;
import com.ideas2it.employee.model.Employee;

/**
 * <p>
 * Gets an input and sets a value 
 * </p>
 */
public class Address {
    private boolean isActive;
    private int addressNo;
    private int employeeId;
    private int clientId;
    private String doorNo;
    private String street;
    private String town;
    private String district;
    private String state; 
    private Client client;
    private Employee employee;
    public void setAddressNo(int addressNo) {
        this.addressNo = addressNo;
    }

    public int getAddressNo() {
        return this.addressNo;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Employee getEmployee() {
        return this.employee;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Client getClient() {
        return this.client;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public int getClientId() {
        return this.clientId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public int getEmployeeId() {
        return this.employeeId;
    }

    public void setDoorNo(String doorNo) {
        this.doorNo = doorNo;
    }

    public String getDoorNo() {
        return this.doorNo;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreet() {
        return this.street;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getTown() {
        return this.town;
    }
    
    public void setDistrict(String district) {
        this.district = district;
    }

    public String getDistrict() {
        return this.district;
    }
    
    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        return this.state;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public boolean getIsActive() {
        return this.isActive;
    }

    /** 
     * <p>
     * Display address as a string by tostring method.
     * </p>
     */
    public String toString() {
        return (new StringBuilder (Constants.DOOR_NO) .append(this.doorNo)
        .append(Constants.STREET).append(this.street).append(Constants.TOWN)
        .append(this.town).append(Constants.DISTRICT).append(this.district)
        .append(Constants.STATE).append(this.state).toString());
    }

}



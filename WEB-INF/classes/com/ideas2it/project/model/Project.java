package com.ideas2it.project.model;

import java.util.List;

import com.ideas2it.client.model.Client;
import com.ideas2it.common.Constants;
import com.ideas2it.employee.model.Employee;

/**
 * <p>
 * Gets an input and sets a value
 * 
 * </p>
 */
public class Project {
    private boolean isActive;
    private int id;
    private int clientId;
    private int employeeId;
    private String name;
    private String description;
    private String domain;
    private List<Employee> employees;
    private Client client;

    public int getId() {
        return this.id;
    }

    public void setId(int projectId) {
        this.id = projectId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String projectName) {
        this.name = projectName;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String projectDescription) {
        this.description = projectDescription;
    }

    public String getDomain() {
        return this.domain;
    }

    public void setDomain(String projectDomain) {
        this.domain = projectDomain;
    }

    public List<Employee> getEmployees() {
        return this.employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }


    public Client getClient() {
        return this.client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public boolean getIsActive() {
        return this.isActive;
    }

    public int getClientId() {
        return this.clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }
    
    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }
   
    public int getEmployeeId() {
        return this.employeeId;
    }   

    /** 
     * <p>
     * Provides Project details as string by toString method
     * </p>
     */
    public String toString() {
        return (new StringBuilder(Constants.PROJECT_ID).append(this.id)
        .append(Constants.NAME).append(this.name).append(Constants.DOMAIN)
        .append(this.domain).append(Constants.DESCRIPTION)
        .append(this.description).toString());
    }

}



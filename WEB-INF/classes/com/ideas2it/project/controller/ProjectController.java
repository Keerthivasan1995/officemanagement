package com.ideas2it.project.controller;

import java.util.ArrayList;
import java.util.List;
 
import javax.servlet.http.HttpServlet;
import org.springframework.stereotype.Controller;  
import org.springframework.web.bind.annotation.ModelAttribute;  
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;  
import org.springframework.web.servlet.ModelAndView; 

import com.ideas2it.common.Constants;
import com.ideas2it.employee.model.Employee;
import com.ideas2it.employee.service.EmployeeService;
import com.ideas2it.employee.service.impl.EmployeeServiceImpl;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.project.model.Project;
import com.ideas2it.project.service.impl.ProjectServiceImpl;
import com.ideas2it.project.service.ProjectService;

/**
 * <p>
 * Project Controller class  provides project update like addition,deletion,
 * update and display of project details.
 *
 * </p>
 * @version          08/09/2017
 * @author           Keerthi vasan.V
 *
 */
@Controller
public class ProjectController extends HttpServlet{
    private ProjectService projectService;
    private EmployeeService employeeService;

    public void setProjectService(ProjectService projectService) {
        this.projectService = projectService;
    }

    public void setEmployeeService(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    /**
     * <p>
     * Provides Addition of project Details
     *
     * </p>
     */
    @RequestMapping(value = "jsp/project_addition", method = RequestMethod.POST) 
    public ModelAndView addProject(@ModelAttribute("project") Project project) {
        ModelAndView modelAndView = null;
        List<Project> projects;
        try {
            projectService.addProject(project);
            projects = projectService.retriveProjects();
            modelAndView = new ModelAndView("projects_display");
            modelAndView.addObject("projects", projects);
            modelAndView.addObject("message", "\t\tProject Details Added SuccessFully\t\t");
        }
        catch (ApplicationException e) {
            modelAndView = new ModelAndView("5xx");
        }
        return modelAndView;
    }     
    /**
     * <p>
     * Provides Deletion of Project
     *
     * </p> 
     *
     */
    @RequestMapping(value = "jsp/project_deletion", method = RequestMethod.GET) 
    public ModelAndView deleteProject(@RequestParam("id") int id) {
        boolean state;
        List<Project> projects;
        ModelAndView modelAndView = null;
        try {
            deassignProjectToEmployees(id); 
            state = projectService.removeProject(id);
            projects = projectService.retriveProjects();
            modelAndView = new ModelAndView("projects_display");
            modelAndView.addObject("projects", projects);
            if (state) {
                modelAndView.addObject("message", "\t\tProject Deleted Successfully\t\t");
            } else {
                modelAndView.addObject("message", "\t\tEntered Project Id Not Exist\t\t");
            }
        }
        catch (ApplicationException e) {
            modelAndView = new ModelAndView("5xx");
        }  
        return modelAndView;
    }

    /**
     * <p>
     * Provides display of list of projects
     *
     * </p>
     */
    @RequestMapping(value = "jsp/projects_display", method = RequestMethod.GET) 
    public ModelAndView displayProjects() {
        ModelAndView modelAndView = null;
        List<Project> projects; 
        try {
            projects = projectService.retriveProjects();
            modelAndView = new ModelAndView("projects_display");
            modelAndView.addObject("projects", projects);
        }
        catch (ApplicationException e) {
            modelAndView = new ModelAndView("5xx");
        }
        return modelAndView;
    }  
  
    /**
     * <p>
     * Provides Updation of project Details
     *
     * </p>
     */
    @RequestMapping(value = "jsp/project_updation",
    params="operation=update", method = RequestMethod.GET) 
    public ModelAndView updateProject(@ModelAttribute("project")
            Project project) {     
        ModelAndView modelAndView = null;
        List<Project> projects;
        try {
            if (project.getEmployees() != null) {
                deassignProjectToEmployees(project, project.getEmployeeId());
            }
            projectService.modifyProject(project);
            projects = projectService.retriveProjects();
            modelAndView = new ModelAndView("projects_display");
            modelAndView.addObject("projects", projects);
            modelAndView.addObject("message", "Project Details Updated Successfully!!!\t\t");
        }
        catch (ApplicationException e) {
            modelAndView = new ModelAndView("5xx");
        }
        return modelAndView;
    }

    /**
     * <p>
     * Provides Project Details based on id 
     *
     * </p>
     */  
    @RequestMapping(value = "jsp/project_details", method = RequestMethod.GET) 
    public ModelAndView getProjectDetails(@RequestParam("id") int id) {
        ModelAndView modelAndView = null;       
        Project project;     
        try {
            project = projectService.getProjectById(id); 
            modelAndView = new ModelAndView("project_details");       
            modelAndView.addObject("project", project);
        }
        catch (ApplicationException e) {
            modelAndView = new ModelAndView("5xx");
        }
        return modelAndView;    
    }

    /**
     * <p>
     * Provides Addition of Employee-project Association Details
     *
     * </p>
     */
    @RequestMapping(value = "jsp/project_emp_assign", 
    params="operation=assignEmployees",method = RequestMethod.GET) 
    public ModelAndView assignEmployeeToProject(@RequestParam("id") int id,
            @RequestParam("employeeId") int employeeId) {
        ModelAndView modelAndView = null;
        List<Employee> employees = new ArrayList<>();
        Employee employee;
        Project project;
        try {
            employee = employeeService.getEmployeeById(employeeId);
            project = projectService.getProjectById(id); 
            employeeService.addProjectToEmployee(employee, project);
            employees.add(employee);
            projectService.addEmployeeToProject(employees, project);
            project = projectService.getProjectById(id); 
            modelAndView = new ModelAndView("project_details");       
            modelAndView.addObject("project", project);
        }
        catch (ApplicationException e) {
            modelAndView = new ModelAndView("5xx");
        }
        return modelAndView;
    }

    /**
     * <p>
     * Provides Deletion of Employee-project Association Details
     *
     * </p>
     */
    public void deassignProjectToEmployees(Project project, int employeeId) 
            throws ApplicationException {
        Employee employee;
        try {
            employee = employeeService.getEmployeeById(employeeId);   
            projectService.deassignEmployeeFromProject
                                                (project, employeeId, employee);
            employeeService.removeProjectToEmployee(employeeId);
        }
        catch (ApplicationException e) {
            throw (e);
        }
    }

    /**
     * <p>
     * Provides list of Employees For Association 
     *
     * </p>
     */
    @RequestMapping(value = "jsp/project_emp_assign", 
    params="operation=getEmployees",method = RequestMethod.GET) 
    public ModelAndView getEmployees(@RequestParam("id") int id) {
        ModelAndView modelAndView = null;
        List<Employee> employees = new ArrayList<>();
        Project project;
        try {

            project = projectService.getProjectById(id);
            employees = employeeService.retriveEmployees();
            modelAndView = new ModelAndView("project_emp_assign");
            modelAndView.addObject("project", project);
            modelAndView.addObject("employees", employees);
        }
        catch (ApplicationException e) {
            modelAndView = new ModelAndView("5xx");
        }
        return modelAndView;
    }  

    /**
     * <p>
     * Provides Details of Project By Id
     * 
     * </p>
     */
    @RequestMapping(value = "jsp/project_updation",
    params="operation=getProjectById", method = RequestMethod.GET) 
    public ModelAndView getProjectById(@RequestParam("id") int id) {    
        Project project;
        ModelAndView modelAndView = null;
        List<Project> projects;
        try {
            project = projectService.getProjectById(id);
            if (project != null) {
                modelAndView = new ModelAndView("project_updation");
                modelAndView.addObject("project", project);             
            } else {
                projects = projectService.retriveProjects();
                modelAndView = new ModelAndView("projects_display");
                modelAndView.addObject("projects", projects);
                modelAndView.addObject("message", "Entered Project Id Not Exist!!!\t\t");            
            }
        }
        catch (ApplicationException e) {
            modelAndView = new ModelAndView("5xx");
        }
        return modelAndView;
    }  

    /**
     * <p>
     * Provides Deletion of Employee-project Association Details
     *
     * </p>
     */
    public void deassignProjectToEmployees(int projectId) 
            throws ApplicationException {
        int employeeId;
        List<Employee> employees = new ArrayList<>();
        Project project;
        try {
            project = projectService.getProjectById(projectId);
            employees = project.getEmployees();
            for (Employee employeetemp : employees) {
                employeeId = employeetemp.getId();
                projectService.deassignEmployeeFromProject
                                           (project, employeeId, employeetemp);
                employeeService.removeProjectToEmployee(employeeId);
            }
        }
        catch (ApplicationException e) {
            throw (e);
        }
    }

}

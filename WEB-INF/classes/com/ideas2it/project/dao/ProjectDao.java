package com.ideas2it.project.dao;

import java.util.List;

import com.ideas2it.client.model.Client;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.project.model.Project;

/**
 * <p>
 * Provides insertion, update, deletion, retrival of Project details 
 * in database 
 * </p>
 */  
public interface ProjectDao {

    /**
     * <p>
     * Provides insertion of Project Details 
     * </p>
     * @param   project
     *          project is object of project details which is passed
     *
     */
    void insertProject(Project project) throws ApplicationException;

    /**
     * <p>
     * Provides Deletion of project Details
     * </p>
     * @param   project
     *          project is object of project details which is passed
     * @return  TRUE If project is deleted | FALSE if Id doesn't Exist 
     */
    boolean deleteProject(Project project) throws ApplicationException;

    /**
     * <p>
     * Provides updation of Project Details into Table
     * </p>
     * @param   project
     *          project is object of project details which is passed
     *          and updated
     */
    void updateProject(Project project) throws ApplicationException;

    /**
     * <p>
     * Provides Retrival of project Details
     * </p>
     * @return  resultSet
     *          which is list of Projects is returned
     */
    List<Project> getProjects() throws ApplicationException;

    /**
     * <p>
     * Provides Retrival of project Details
     * </p>
     * @return  resultSet
     *          which is object of Project is returned
     */
    Project getProjectById(int targetId) throws ApplicationException;

}

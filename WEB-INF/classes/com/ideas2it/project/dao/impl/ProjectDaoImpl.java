package com.ideas2it.project.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.HibernateException; 
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.ideas2it.client.model.Client;
import com.ideas2it.common.Constants;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.logger.LoggerManager;
import com.ideas2it.project.dao.ProjectDao;
import com.ideas2it.project.model.Project;

/**
 * <p>
 * ProjectDaoimplement class implements ProjectDao Interface and 
 * serves CRUD operation (create/read/update/delete) operation
 * </p>
 */
public class ProjectDaoImpl implements ProjectDao{

    private static final LoggerManager loggerManager
               = new LoggerManager(ProjectDaoImpl.class.getName());

    private HibernateTemplate hibernateTemplate;
    
    public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
        this.hibernateTemplate = hibernateTemplate;
    }

    /**
     * @see    com.ideas2it.project.dao.ProjectDao;
     * @Method insertProject
     */
    public void insertProject(Project project) throws ApplicationException {
        try {
            hibernateTemplate.save(project); 
        } 
        catch (HibernateException e) {
            String msg = (new StringBuilder(Constants.ERROR_ADD)
            .append(Constants.NAME).append(project.getName()).toString());
            loggerManager.logError(msg, e);
            throw new ApplicationException(msg);
        }
    }

    /**
     * @see    com.ideas2it.project.dao.ProjectDao;
     * @Method deleteProject
     */
    public boolean deleteProject(Project project) throws ApplicationException {
        try {
            hibernateTemplate.update(project);   
            return Boolean.TRUE;
        } 
        catch (HibernateException e) {
            String msg = (new StringBuilder(Constants.ERROR_DELETE)
            .append(Constants.PROJECT_ID).append(project.getId()).toString());
            loggerManager.logError(msg, e);
            throw new ApplicationException(msg);
        }
    }

    /**
     * @see    com.ideas2it.project.dao.ProjectDao;
     * @Method updateProject
     */
    public void updateProject(Project project) throws ApplicationException {
        try {
            hibernateTemplate.update(project);
        }
        catch (HibernateException e) {
            String msg = (new StringBuilder(Constants.ERROR_UPDATE)
            .append(Constants.PROJECT_ID).append(project.getId()).toString());
            loggerManager.logError(msg, e);
            throw new ApplicationException(msg);
        }     
    }

    /**
     * @see    com.ideas2it.project.dao.ProjectDao;
     * @Method gettProject
     */
    public List<Project> getProjects() throws ApplicationException {
        List<Project> projects;
        DetachedCriteria detachedCriteria;
        try {
            detachedCriteria = DetachedCriteria.forClass(Project.class);
            detachedCriteria.add(Restrictions.eq(Constants.ISACTIVE, Boolean.TRUE));
            projects =  (List<Project>) hibernateTemplate
            .findByCriteria(detachedCriteria);
        }
        catch (HibernateException e) {
            loggerManager.logError(Constants.ERROR_RETRIVE, e);
            throw new ApplicationException(Constants.ERROR_RETRIVE);
        } 
        return projects;    
    }

    /**
     * @see    com.ideas2it.project.dao.ProjectDao;
     * @Method getProjectById
     */
    public Project getProjectById(int targetId) throws ApplicationException {
        Project project = null;
        List<Project> projects;
        DetachedCriteria detachedCriteria;
        try {
            detachedCriteria = DetachedCriteria.forClass(Project.class);
            detachedCriteria.add(Restrictions.eq(Constants.ISACTIVE, Boolean.TRUE))
                .add(Restrictions.eq(Constants.ID,targetId));
            projects =  (List<Project>) hibernateTemplate
            .findByCriteria(detachedCriteria);
            if(!projects.isEmpty()) {
                project = projects.get(0);
            }
            return project;
        }
        catch (HibernateException e) {
            String msg = (new StringBuilder(Constants.ERROR_RETRIVE)
            .append(Constants.PROJECT_ID).append(targetId).toString());
            loggerManager.logError(msg, e);
            throw new ApplicationException(msg);
        } 
    }

}

package com.ideas2it.project.service;

import java.util.List;

import com.ideas2it.client.model.Client;
import com.ideas2it.employee.model.Employee;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.project.model.Project;

/**
 * <p>
 * Provides control of an addition , deletion , update ,
 * and Display the details of project.
 * </p>
 */
public interface ProjectService {
 
    void addProject(Project project) throws ApplicationException;

    /**
     * <p>
     * Deletes project details by Project ID 
     *
     * </p>
     * @param         project ID is validated
     *
     */
    boolean removeProject(int targetId) throws ApplicationException;

    /**
     * <p>
     * Display list of Projects
     * 
     * </p>
     */ 
    List<Project> retriveProjects() throws ApplicationException;

    /**
     * <p>
     * Updates the project list
     *
     * </p> 
     * @param         project
     *                object of project is passed and updated
     *
     */
    void modifyProject(Project project) throws ApplicationException;

    /**
     * <p>
     * Provides an project object based on Project Id
     * 
     *</p>
     * @return       Project object is returned
     */ 
    Project getProjectById(int projectId) throws ApplicationException; 

    /**
     * <p>
     * Provides association of Employees to project list.
     *
     * </p>
     * @param   employee list and project object 
     *          is passed 
     *
     */  
    void addEmployeeToProject(List<Employee> employees, Project project)
            throws ApplicationException;

    /**
     * <p>
     * Provides Unassign of Employees to project list.
     *
     * </p>
     * @param   employee object and project object 
     *          is passed 
     *
     */ 
    void deassignEmployeeFromProject(Project project, int employeeId,
            Employee employee) throws ApplicationException;

    /**
     * <p>
     * Provides association of Client to project 
     *
     * </p>
     * @param   Client object and project object 
     *          is passed 
     *
     */ 
    void addClientToProject(Project project, Client client)
            throws ApplicationException; 

   /**
     * <p>
     * Provides deassign of Client to project 
     *
     * </p>
     * @param   Client object and project object 
     *          is passed 
     *
     */ 
    public void removeClientToProject(Project project)
            throws ApplicationException;

}

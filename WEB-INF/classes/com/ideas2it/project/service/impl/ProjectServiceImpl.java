package com.ideas2it.project.service.impl;

import java.util.List;

import com.ideas2it.client.model.Client;
import com.ideas2it.common.Constants;
import com.ideas2it.employee.model.Employee;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.project.dao.impl.ProjectDaoImpl;
import com.ideas2it.project.dao.ProjectDao;
import com.ideas2it.project.model.Project;
import com.ideas2it.project.service.ProjectService;

/**
 * <p>
 * ProjectServiceimplement class implements ProjectService Interface and 
 * serves CRUD operation (create/read/update/delete) operation
 * </p>
 */
public class ProjectServiceImpl implements ProjectService {

    private ProjectDao projectDao;
    
    public void setProjectDao(ProjectDao projectDao) {
        this.projectDao = projectDao;
    }

    /**
     * @see    com.ideas2it.project.service.ProjectService;
     * @Method addProject
     */
    public void addProject(Project project) throws ApplicationException {
        project.setIsActive(Boolean.TRUE);
        projectDao.insertProject(project);
    }

    /**
     * @see    com.ideas2it.project.service.ProjectService;
     * @Method removeProject
     */
    public boolean removeProject(int targetId) throws ApplicationException {
        Project project = null;  
        project = projectDao.getProjectById(targetId);
        if (project != null) {
            project.setIsActive(Boolean.FALSE);
            return projectDao.deleteProject(project);
        } else {
            return Boolean.FALSE;
        }
    }

    /**
     * @see    com.ideas2it.project.service.ProjectService;
     * @Method retriveProject
     */
    public List<Project> retriveProjects() throws ApplicationException {
        return projectDao.getProjects();
    }

    /**
     * @see    com.ideas2it.project.service.ProjectService;
     * @Method modifyProject
     */
    public void modifyProject(Project project) throws ApplicationException {
        project.setIsActive(Boolean.TRUE);
        projectDao.updateProject(project);
    }

    /**
     * @see    com.ideas2it.project.service.ProjectService;
     * @Method getProjectById
     */
    public Project getProjectById(int projectId) throws ApplicationException {
        Project project = null;  
        project = projectDao.getProjectById(projectId);
        if (project != null) {
            return project;
        }
        return null;
   } 

    /**
     * @see    com.ideas2it.project.service.ProjectService;
     * @Method addEmployeeToProject
     */ 
    public void addEmployeeToProject(List<Employee> employees, Project project)
            throws ApplicationException {        
        project.setIsActive(Boolean.TRUE);
        project.setEmployees(employees);
        projectDao.updateProject(project);
    }

    /**
     * @see     com.ideas2it.project.service.ProjectService
     * @method  deassignEmployeeFromProject
     */
    public void deassignEmployeeFromProject(Project project, int employeeId,
            Employee employee) throws ApplicationException {
        Object temp = null;
        List<Employee> employees;
        if ((null != employee) && (null != project)) {
            employees = project.getEmployees();
            for (Employee employeetemp : employees) {
                if (employeetemp.getId() == employeeId) {
                    temp = employeetemp;
                }
            }
            employees.remove(temp);
            project.setIsActive(Boolean.TRUE);
            project.setEmployees(employees);
            projectDao.updateProject(project);
        }            
    }

    /**
     * @see    com.ideas2it.project.service.ProjectService;
     * @Method addClientToProject
     */ 
    public void addClientToProject(Project project, Client client)
            throws ApplicationException {        
        project.setIsActive(Boolean.TRUE);
        project.setClient(client);
        projectDao.updateProject(project);
    }

    /**
     * @see    com.ideas2it.project.service.ProjectService;
     * @Method removeClientToProject
     */ 
    public void removeClientToProject(Project project)
            throws ApplicationException {
        project.setIsActive(Boolean.TRUE);
        project.setClient(null);
        projectDao.updateProject(project);
        
    }

}

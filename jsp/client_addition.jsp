<%@ include file="header.jsp"%>
<head>
<style>
table {
    border-collapse: collapse;
    width: 90%;
}

input[type=text], input[type=email] {
    width: 60%;
    padding: 8px 30px;
}

th, td {
    text-align: left;
    padding: 8px;
    height:200;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: red;
    color: white;
}
.button {
    background-color: #000000;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    font-size: 16px;
    margin: 4px 2px;
}
.button:hover{
    background-color: red;
}
</style>
<title>Client Registeration</title>
</head>
<body>
<h2>Client Registeration</h2>
<form method="POST" action="/officemanagement/jsp/client_addition" >
    <table style="width: 100%">
        <tr>
            <td>Client Name</td>
            <td><input type="text" name="name" maxlength="15" pattern="[A-Za-z]*"
             placeholder="Only Characters[Eg:qqqq]" required ></td>
        </tr>
        <tr>
            <td>MailId</td>
            <td><input type="email" name="mailId" maxlength="30" type="email" placeholder="Eg:aaa@aaa.aa" 
            required /></td>
        </tr>
        <td style="font-weight: bold">Address Details</td>
        <tr>
            <td>Door No</td>
            <td><input type="text" name="doorNo"  maxlength="25" placeholder="Eg:12/122A" required /></td>
        </tr>
        <tr>
            <td>Street</td>
            <td><input type="text" name="street" maxlength="25" pattern="[A-Za-z]*"
              placeholder="Eg:aaaaaa" required /></td>
        </tr>
        <tr>
            <td>Town</td>
            <td><input type="text" name="town" maxlength="25" pattern="[A-Za-z]*"
              placeholder="Eg:aaaaaa" required /></td>
        </tr>
        <tr>
            <td>District</td>
            <td><input type="text" name="district"maxlength="25" pattern="[A-Za-z]*"
              placeholder="Eg:aaaaaa" required /></td>
        </tr>
        <tr>
            <td>State</td>
            <td><input type="text" name="state" maxlength="25" pattern="[A-Za-z]*"
              placeholder="Eg:aaaaaa" required /></td>
        </tr>
   </table>
    <input type="hidden" name="operation" value="create"> 
    <center> <input value="Add" type="submit" class="button" >
        <input type="reset" class="button">
    </center>
</form>
<%@ include file="footer.jsp"%>
</body>
</html>

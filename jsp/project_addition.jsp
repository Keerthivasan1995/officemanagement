<%@ include file="header.jsp"%>
<head>
<style>
table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: red;
    color: white;
}
.button {
    background-color: #000000;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    font-size: 16px;
    margin: 4px 2px;
}
.button:hover{
    background-color: red;
}
input[type=text], input[type=email] {
    width: 60%;
    padding: 8px 30px;
}
</style>
<title>Project Registeration</title>
</head>
<body>
<h2>Project Registeration</h2>
<form  method="POST" action="/officemanagement/jsp/project_addition">
    <table style="with: 100%">
        <tr>
            <td>Project Name</td>
            <td><input type="text" name="name" maxlength="15"  pattern="[A-Za-z]*"
             placeholder="Only Characters[Eg:qqqq]" required ></td>
        </tr>
        <tr>
            <td>Domain</td>
            <td><input type="text" name="domain"  maxlength="15"
             placeholder="Enter Domain" required ></td>
        </tr>
        <tr>
            <td>Description</td>
            <td><textarea name = description rows="4"  maxlength="150">
             </textarea></td>
        </tr>        
   </table>
    <input type="hidden" name="operation" value="create"> 
    <center> <input value="Add" type="submit" class="button" >
        <input type="reset" class="button" >
    </center>
</form>
</body>
</html>
<%@ include file="footer.jsp"%>

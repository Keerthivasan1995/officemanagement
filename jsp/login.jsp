<html>
<head>
<style>
input[type=text], input[type=password] {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
}

.button {
    background-color: #000000;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    font-size: 16px;
    margin: 4px 2px;
}
.button:hover{
    background-color: red;
}
body {
    background-image: url("/officemanagement/images/Grey And Orange Background In Office.jpg");
}
table{
    background-color: #C0C0C0;
    align: center;
    margin:auto;
    padding: 15px 32px;
}
</style>
<body>
<title>Login Page</title>
</head>
<body>
<script type="text/javascript">
     if ("${message}" != "") {
        alert("${message}");
     }
</script> 
<form method="post" action="/officemanagement/jsp/login">
    <div>
        <table style="margin-top:200px">
            <tr>
                <td>User Name:</td>
                 <td><input type="text" name="userName"  placeholder="Username" required maxlength="20"/><td>
            </tr>
            <tr>
                 <td>Password:</td>
                 <td><input type="password" name="password"  placeholder="Password" required maxlength="30"/><td>
            </tr>
            <td><input value="login" type="submit" class="button"></td>
        </table> 
    </div>
</form>
</body>
</html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ include file="header.jsp"%>
<html>
<head>
<style>
table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: red;
    color: white;
}
</style>
</style>
<title>Employee Details</title>
</head>
<body>
<form>
    <table style="with: 100%">
        <tr>
            <td>Employee Name</td>
            <td>${employee.name}</td>
        </tr>
        <tr>
            <td>MailId</td>
            <td>${employee.mailId}</td>
        </tr>
        <tr>
            <td>Date Of Birth</td>
            <td>${employee.dob}</td>
        </tr>
        <tr>
            <td>Date Of Joining</td>
            <td>${employee.doj}</td>
        </tr>
        <tr>
            <td>Project Id</td>
            <td>${employee.project.id}</a></td> 
        </tr>  
        <c:forEach items="${employee.addresses}" var="address">
            <tr>
                <td>Door No</td>
                <td>${address.doorNo}</td>
            </tr>
            <tr>
                <td>Street</td>
                <td>${address.street}</td>
            </tr>
            <tr>
                <td>Town</td>
                <td>${address.town}</td>
            </tr>
            <tr>
                <td>District</td>
                <td>${address.district}</td>
            </tr>
            <tr>
                <td>State</td>
                <td>${address.state}</td>
            </tr>
        </c:forEach>     
    </table>
</form>
</body>
</html>
<%@ include file="footer.jsp"%>

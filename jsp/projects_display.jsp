<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ include file="header.jsp"%>
<html>
<head>
<style>
table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: red;
    color: white;
}
</style>
<title>List Of Projects</title>
</head>
<body>
<script type="text/javascript">
     if ("${message}" != "") {
        alert("${message}");
     }
</script>
<h2>List Of Projects</h2>
<form  method="GET" action="/officemanagement/jsp/projects_display">
    <table style="with: 100%">
        <tr>
            <th>Project Id</th>
            <th>Project Name</th>
            <th>Domain</th>
            <th>Description</th>
            <th>Employee Association</th>
            <th>Action</th>
        </tr>
        <c:forEach items="${projects}" var="project">
            <tr>
                <td><a href="./project_details?id=${project.id}&operation=getProjectDetails">
                ${project.id}</a></td> 
                <td>${project.name}</td>   
                <td>${project.domain}</td> 
                <td>${project.description}</td>
                <td><a href="./project_emp_assign?id=${project.id}&operation=getEmployees">
                    Assign</a></td>
                <td><a onclick="return confirm('\t\t\tAre You Sure You Want To Delete?\n\n If You Delete, Employees Will Be Unassigned From Current Working Project!')" href="./project_deletion?id=${project.id}&operation=delete"/>Delete</a>
                <a href="./project_updation?id=${project.id}&operation=getProjectById">Update</a></td> 
            </tr>   
        </c:forEach>
    </table>
</form>
</body>
</html>
<%@ include file="footer.jsp"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ include file="header.jsp"%>
<html>
<head>
<style>
table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: red;
    color: white;
}
</style>
</style>
<title>Client Details</title>
</head>
<body>
<form>
    <table style="with: 100%">
        <tr>
            <td>Client Name</td>
            <td>${client.name}</td>
        </tr>
        <tr>
            <td>mailId</td>
            <td>${client.mailId}</td>
        </tr>
        <c:forEach items="${client.addresses}" var="address">
            <tr>
                <td>Door No</td>
                <td>${address.doorNo}</td>
            </tr>
            <tr>
                <td>Street</td>
                <td>${address.street}</td>
            </tr>
            <tr>
                <td>Town</td>
                <td>${address.town}</td>
            </tr>
            <tr>
                <td>District</td>
                <td>${address.district}</td>
            </tr>
            <tr>
                <td>State</td>
                <td>${address.state}</td>
            </tr>
    </c:forEach>      
    </table>
    <h3>Projects Associated</h3>
    <table style="with: 100%">
        <tr>
           <th>Project Id</th>
           <th>Project Name</th>
           <th>Domain</th>
           <th>Description</th>
       </tr>
       <c:forEach items="${client.projects}" var="project" >
           <tr>
                <td>${project.id}</a></td> 
                <td>${project.name}</td>   
                <td>${project.domain}</td> 
                <td>${project.description}</td>
            </tr>   
        </c:forEach>
    </table>
</form>
</body>
</html>
<%@ include file="footer.jsp"%>

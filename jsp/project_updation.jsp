<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ include file="header.jsp"%>
<html>
<head>
<style>
table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: red;
    color: white;
}
.button {
    background-color: #000000;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    font-size: 16px;
    margin: 4px 2px;
}
.button:hover{
    background-color: red;
}
input[type=text], input[type=email] {
    width: 60%;
    padding: 8px 30px;
}
</style>
<title>Project Updation</title>
</head>
<body>
<h2>Project updation</h2>
<form  method="GET" action="/officemanagement/jsp/project_updation">
    <table style="with: 100%">
        <tr>
            <td>Project Id</td>
            <td><input type="int" name="id" value = "${project.id}" required 
             maxlength="5" placeholder="Eg:0000"pattern="[0-9]*" /></td>
        </tr>
    </table>
    <input type="hidden" name="operation" value="getProjectById">
    <input type="submit" value="Get" class="button" />
</form>
<form>
    <table style="with: 100%">
        <tr>
            <td>Project Name</td>
            <td><input type="text" name="name" value = "${project.name}"  required 
             maxlength="25" placeholder="(Only Charachers)Eg:aaaa" pattern="[A-Za-z]*" /></td>
        </tr>
        <tr>
            <td>Domain</td>
            <td><input type="text" name="domain" value = "${project.domain}" maxlength="25"
                 pattern="[A-Za-z]*" placeholder="Eg:aaaaaa" required /></td>
        </tr>
        <tr>
            <td>Description</td>
            <td><textarea name = description rows="4" cols="50" 
                value = "${project.description}" maxlength="150" />
            </textarea></td>
        </tr>
        <tr>
            <td>Un-Assign Employee</td>
            <td><select name="employeeId" size="1" width="100">
                <option value="" disabled selected>Select your option</option>
                <c:forEach items="${project.employees}" var="employee" >
                    <option value="${employee.id}">${employee.name}</option>
                </c:forEach>
                </select> 
           </td>
       </tr>  
    </table>
    <input type="hidden" name="id" value = "${project.id}" />
    <input type="hidden" name="operation" value="update"> 
    <center> <input value="Update" type="submit" class="button">
        <input type="reset" class="button"> 
    </center>
</form>
</body>
</html>
<%@ include file="footer.jsp"%>

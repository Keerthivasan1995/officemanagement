<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ include file="header.jsp"%>
<html>
<head>
<style>
table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: red;
    color: white;
}
</style>
</style>
<title>Project Details</title>
</head>
<h2>Project Details</h2>
<body>
<form>
    <table style="with: 100%">
        <tr>
            <td>Project Name</td>
            <td>${project.name}</td>
        </tr>
        <tr>
            <td>Domain</td>
            <td>${project.domain}</td>
        </tr>
        <tr>
            <td>Description</td>
            <td>${project.description}</td>
        </tr> 
        <tr>
            <td>Client Id</td>
            <td>${project.client.getId()}</td>
        </tr> 
    </table>
    <h3>Employees Associated</h3>
    <table style="with: 100%">
        <tr>
           <th>Employee Id</th>
           <th>Employee Name</th>
           <th>MailID</th>
           <th>Date Of Birth</th>
           <th>Date Of joining</th>
       </tr>
       <c:forEach items="${project.employees}" var="employee" >
           <tr>
                <td>${employee.id}</a></td> 
                <td>${employee.name}</td>   
                <td>${employee.mailId}</td> 
                <td>${employee.dob}</td>
                <td>${employee.doj}</td>
            </tr>   
        </c:forEach>
    </table>
</form>
</body>
</html>
<%@ include file="footer.jsp"%>

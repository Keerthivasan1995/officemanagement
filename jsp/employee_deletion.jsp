<%@ include file="header.jsp"%>
<html>
<head>
<style>
table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: red;
    color: white;
}
.button {
    background-color: #000000;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    font-size: 16px;
    margin: 4px 2px;
}
.button:hover{
    background-color: red;
}
input[type=text], input[type=email] {
    width: 60%;
    padding: 8px 30px;
}
</style>
<title>Employee Deletion</title>
</head>
<body>
<h2>Employee Deletion</h2>
<form method="GET" action="/officemanagement/jsp/employee_deletion">
    <table style="with: 100%">
        <tr>
            <td>Employee Id</td>
            <td><input type="int" name="id" value = "${employee.id}" required 
             maxlength="5" placeholder="Eg:0000"pattern="[0-9]*" /></td>
        </tr>
   </table>
   <center><input onclick="return confirm('\t\t\tAre You Sure You Want To Delete?\n\n If You Delete, Employee   Will Be Unassigned From Current Working Project!')" value="delete" name="operation" type="submit" class="button"></center>
   </form>
</body>
</html>
<%@ include file="footer.jsp"%>

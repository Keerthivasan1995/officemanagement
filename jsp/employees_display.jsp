<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ include file="header.jsp"%>
 <link rel="stylesheet" type="text/css" href="/recursos/estilos/test.css" media="all" />
<html>
<head>
<style>
table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: red;
    color: white;
}
</style>
<title>List Of Employees</title>
</head>
<body>
<script type="text/javascript">
     if ("${message}" != "") {
        alert("${message}");
     }
</script>
<h2>List Of Employees</h2>
<form method="GET" action="/officemanagement/jsp/employees_display">
    <table style="with: 100%">
        <tr>
           <th>Employee Id</th>
           <th>Employee Name</th>
           <th>MailID</th>
           <th>Date Of Birth</th>
           <th>Date Of joining</th>
           <th>Action</th>
       </tr>
       <c:forEach items="${employees}" var="employee" >
           <tr>
                <td><a href="./employee_details?id=${employee.id}&operation=getEmployeeDetails">
                ${employee.id}</a></td> 
                <td>${employee.name}</td>   
                <td>${employee.mailId}</td> 
                <td>${employee.dob}</td>
                <td>${employee.doj}</td>
                <td><a onclick="return confirm('\t\t\tAre You Sure You Want To Delete?\n\n If You Delete, Employee Will Be Unassigned From Current Working Project!')" href="./employee_deletion?id=${employee.id}&operation=delete"/>Delete</a>
                <a href="./employee_updation?id=${employee.id}&operation=getEmployeeById">Update</a></td> 
            </tr>   
        </c:forEach>
    </table>
</form>
</body>
</html>
<%@ include file="footer.jsp"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ include file="header.jsp"%>
<html>
<head>
<style>
table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: red;
    color: white;
}
</style>
<title>List Of Clients</title>
</head>
<body>
<script type="text/javascript">
     if ("${message}" != "") {
        alert("${message}");
     }
</script>
<h2>List Of Clients</h2>
<form method="GET" action="/officemanagement/jsp/clients_display">
    <table style="with: 100%">
        <tr>
            <th>Client Id</th>
            <th>Client Name</th>
            <th>MailId</th>
            <th>Project Association</th>
            <th>Action</th>
        </tr>
        <c:forEach items="${clients}" var="client">
            <tr>
                <td><a href="./client_details?id=${client.id}&operation=getClientDetails">
                ${client.id}</a></td> 
                <td>${client.name}</td>   
                <td>${client.mailId}</td> 
                <td><a href="./client_project_assign?id=${client.id}&operation=getProjects">
                    Assign</a></td>
                 <td><a onclick="return confirm('\t\t\tAre You Sure You Want To Delete?\t\t\t')" href="./client_deletion?id=${client.id}&operation=delete"/>Delete</a> 
                <a href="./client_updation?id=${client.id}&operation=getClientById">Update</a></td> 
            </tr>   
        </c:forEach>
    </table>
</form>
</body>
</html>
<%@ include file="footer.jsp"%>

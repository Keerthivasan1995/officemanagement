<%@ include file="header.jsp"%>
 <link rel="stylesheet" type="text/css" href="/recursos/estilos/test.css" media="all" />
<html>
<head>
<style>
table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    text-align: left;
    padding: 3px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: red;
    color: white;
}
.button {
    background-color: #000000;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    font-size: 16px;
    margin: 4px 2px;
}
.button:hover{
    background-color: red;
}
input[type=text], input[type=email] {
    width: 50%;
    padding: 6px 20px;
}
</style>
<title>Employee Registeration</title>
</head>
<body>
<h3>Employee Registeration</h3>
<form method="POST" action="/officemanagement/jsp/employee_addition" >
    <table style="width: 100%">
        <tr>
            <td>Employee Name</td>
            <td><input type="text" name="name" maxlength="15"  pattern="[A-Za-z]*"
             placeholder="Only Characters[Eg:aaaa]" required ></td>
        </tr>
         
        <tr>
            <td>MailId</td>
            <td><input type="email" name="mailId" maxlength="30" type="email" placeholder="Eg:aaa@aaa.aa" 
            required /></td>
        </tr>
        <tr>
            <td>Date Of Birth (DD/MM/YYYY)</td>
            <td><input type="text" name="dob" placeholder="Eg:12/12/1995"
             pattern ="[0-3][0-9]/[0-1][1-9]/[1-2][0-9][0-9][0-9]" maxlength="10" required /></td>
        </tr>
        <tr>
            <td>Date Of Joining (DD/MM/YYYY)</td>
            <td><input type="text" name="doj"  placeholder="Eg:12/12/1995"
             pattern ="[0-3][0-9]/[0-1][1-9]/[1-2][0-9][0-9][0-9]" maxlength="10" required /></td>
        </tr>
        <td style="font-weight: bold">Address Details </td>
        <tr>
            <td>Door No</td>
            <td><input type="text" name="doorNo" maxlength="15"  placeholder="Eg:12/122A" required /></td>
        </tr>
        <tr>
            <td>Street</td>
            <td><input type="text" name="street" maxlength="25" pattern="[A-Za-z]*"
              placeholder="Eg:aaaaaa" required /></td>
        </tr>
        <tr>
            <td>Town</td>
            <td><input type="text" name="town" maxlength="25" pattern="[A-Za-z]*"
             placeholder="Eg:aaaaaa" required /></td>
        </tr>
        <tr>
            <td>District</td>
            <td><input type="text" name="district" maxlength="25" pattern="[A-Za-z]*" 
            placeholder="Eg:aaaaaa"  required /></td>
        </tr>
        <tr>
            <td>State</td>
            <td><input type="text" name="state" maxlength="25" placeholder="Eg:aaaaaa"  required /></td>
        </tr>
    </table>
    <input type="hidden" name="operation" value="create"> 
    <center> <input value="Add" type="submit" class="button" >
        <input type="reset" class="button">
    </center>
</form>
</body>
</html>
<%@ include file="footer.jsp"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ include file="header.jsp"%>
<html>
<head>
<style>
table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: red;
    color: white;
}
.button {
    background-color: #000000;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    font-size: 16px;
    margin: 4px 2px;
}
.button:hover{
    background-color: red;
}
input[type=text], input[type=email] {
    width: 60%;
    padding: 8px 30px;
}
</style>
<title>Client Updation</title>
</head>
<body>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
       <script>
           $(document).ready(function(){
               $("button").click(function(){
                   $("#getform").toggle();
                   $("#updateform").toggle();
               });
           });
       </script>
<h2>Client updation</h2>
<form method="GET" id = "getform" class= "hello" action="/officemanagement/jsp/client_updation">
    <table style="with: 100%">
        <tr>
            <td>Client Id</td>
            <td><input type="integer" name="id" value = "${client.id}" required 
             maxlength="5" placeholder="Eg:0000"pattern="[0-9]*" /></td>
        </tr>
    </table>
    <input type="hidden" name="operation" value="getClientById">
    <input type="submit" value="Get" class="button"/>
</form>
<form id = "updateform" class= "hello"  >
    <table style="with: 100%">
        <tr>
            <td>Client Name</td>
            <td><input type="text" name="name" value = "${client.name}"  required 
             maxlength="25" placeholder="(Only Charachers)Eg:aaaa" pattern="[A-Za-z]*" /></td>
        </tr>
        <tr>
            <td>MailId</td>
            <td><input type="email" name="mailId" value = "${client.mailId}" maxlength="30"
             placeholder="Eg:aaa@aaa.aa" required /></td>
        </tr>
        <c:forEach items="${client.addresses}" var="address">
            <tr>
                <td>Door No</td>
                <td><input type="text" name="doorNo" value = "${address.doorNo}"  maxlength="15" 
                 placeholder="Eg:12/122A" required /></td>
            </tr>
            <tr>
                <td>Street</td>
                <td><input type="text" name="street" value = "${address.street}" maxlength="25"
                 pattern="[A-Za-z]*" placeholder="Eg:aaaaaa" required /></td>
            </tr>
            <tr>
                <td>Town</td>
                <td><input type="text" name="town" value = "${address.town}" maxlength="25"
                 pattern="[A-Za-z]*" placeholder="Eg:aaaaaa" required /></td>
            </tr>
            <tr>
                <td>District</td>
                <td><input type="text" name="district" value = "${address.district}"maxlength="25"
                 pattern="[A-Za-z]*" placeholder="Eg:aaaaaa" required /></td>
            </tr>
            <tr>
                <td>State</td>
                <td><input type="text" name="state" value = "${address.state}" maxlength="25"
                 pattern="[A-Za-z]*" placeholder="Eg:aaaaaa" required /></td>
            </tr>
        </c:forEach>    
        <tr>
            <td>Un-Assign Project</td>
            <td><select name="projectId" size="1">
                <option value="" disabled selected>Select your option</option>
                <c:forEach items="${client.projects}" var="project" >              
                    <option value="${project.id}">${project.name}</option>
                </c:forEach>
                </select> 
           </td>
       </tr> 
    </table>
    <input type="hidden" name="id" value = "${client.id}" />
    <center> <input name="operation" value="update" type="submit" class="button">
        <input type="reset" class="button"> 
    </center>
</form>
</body>
</html>
<%@ include file="footer.jsp"%>

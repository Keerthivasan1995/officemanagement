<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ include file="header.jsp"%>
<head>
<style>
table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: red;
    color: white;
}
.button {
    background-color: #000000;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    font-size: 16px;
    margin: 4px 2px;
}
.button:hover{
    background-color: red;
}
input[type=text], input[type=email] {
    width: 60%;
    padding: 8px 30px;
}
</style>
<title>Project-Client Assign</title>
</head>
<body>
<h2>Project-Client Assign</h2>
<form method="GET" action="/officemanagement/jsp/client_project_assign" >
    <table style="with: 100%">
        <tr>
            <td>Client Id</td>
            <td><input type="int" name="id" value = "${client.id}" readonly/></td>
       </tr>
        <tr>
            <td>Project Id</td>
            <td><select name="projectId" size="1">
                <option value="" disabled selected>Select your option</option>
                <c:forEach items="${projects}" var="project" >              
                    <option value="${project.id}">${project.name}</option>
                </c:forEach>
                </select> 
           </td>   
        </tr>
   </table>       
   <input type = "hidden" name ="operation" value = "assignProjects">
   <input type="submit" value="Assign" class="button"/>
</form>
</body>
</html>
<%@ include file="footer.jsp"%>

<!DOCTYPE html>
<html>
<head>
<style>
ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: #333;
}

li {
    float: left;
}

lix {
    float: right;
}

li a, .dropbtn {
    display: inline-block;
    color: white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}

li a:hover, .dropdown:hover .dropbtn {
    background-color: red;
}

li.dropdown {
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

.dropdown-content a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
    text-align: left;
}

.dropdown-content a:hover {background-color: #f1f1f1}

.dropdown:hover .dropdown-content {
    display: block;
}

</style>
</head>
<body>

<ul>
  <li><a href="/officemanagement/jsp/index.jsp">Home</a></li>
  <li class="dropdown">
    <a href="javascript:void(0)" class="dropbtn">Employee</a>
    <div class="dropdown-content">
        <a href="./employee_addition.jsp">Employee Creation</a><br> 
        <a href="./employee_deletion.jsp">Employee Deletion</a><br> 
        <a href="./employees_display?operation=display">Employee View</a><br>
        <a href="./employee_updation.jsp">Employee Updation</a><br>
    </div>
  </li>
  <li class="dropdown">
    <a href="javascript:void(0)" class="dropbtn">Project</a>
    <div class="dropdown-content">
        <a href="./project_addition.jsp">Project Creation</a><br> 
        <a href="./project_deletion.jsp">Project Deletion</a><br> 
        <a href="./projects_display?operation=display">Project View</a><br>
        <a href="./project_updation.jsp">Project Updation</a><br>
    </div>
  </li>
  <li class="dropdown">
    <a href="javascript:void(0)" class="dropbtn">Client</a>
    <div class="dropdown-content">
        <a href="./client_addition.jsp">Client Creation</a><br> 
        <a href="./client_deletion.jsp">Client Deletion</a><br> 
        <a href="./clients_display?operation=display">Client View</a><br>
        <a href="./client_updation.jsp">Client Updation</a><br> 
    </div>
  </li>
  <lix><%@ include file="logout.jsp"%></a></lix>
</ul>
</body>
</html>
</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ include file="header.jsp"%>
<html>
<head>
<style>
table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: red;
    color: white;
}
.button {
    background-color: #000000;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    font-size: 16px;
    margin: 4px 2px;
}
.button:hover{
    background-color: red;
}
input[type=text], input[type=email] {
    width: 60%;
    padding: 8px 30px;
}
</style>
<title>Employee Updation</title>
</head>
<body>
<h2>Employee updation</h2>
<form method="GET" action="/officemanagement/jsp/employee_updation">
    <table style="with: 100%">
        <tr>
            <td>Employee Id</td>
            <td><input type="integer" name="id" value = "${employee.id}" required 
             maxlength="5" placeholder="Eg:0000"pattern="[0-9]*" /></td>
        </tr>
    </table>
    <input type="hidden" name="operation" value="getEmployeeById">
    <input type="submit" value="Get" class="button" />
</form>
<form>
    <table style="with: 100%">
        <tr>
            <td>Employee Name</td>
            <td><input type="text" name="name" value = "${employee.name}"  required 
             maxlength="25" placeholder="(Only Charachers)Eg:aaaa" pattern="[A-Za-z]*" /></td>
        </tr>
        <tr>
            <td>MailId</td>
            <td><input type="email" name="mailId" value = "${employee.mailId}" maxlength="30"
             placeholder="Eg:aaa@aaa.aa" required /></td>
        </tr>
        <tr>
            <td>Date Of Birth</td>
            <td><input type="text" name="dob" value = "${employee.dob}"  placeholder="Eg:12/12/1995"
             pattern ="[0-3][0-9]/[0-1][1-9]/[1-2][0-9][0-9][0-9]" maxlength="10" required /></td>
        </tr>
        <tr>
            <td>Date Of Joining</td>
            <td><input type="text" name="doj"value = "${employee.doj}"  placeholder="Eg:12/12/1995"
             pattern ="[0-3][0-9]/[0-1][1-9]/[1-2][0-9][0-9][0-9]" maxlength="10" required /></td>
        </tr>
         <c:forEach items="${employee.addresses}" var="address">
            <tr>
                <td>Door No</td>
                <td><input type="text" name="doorNo" value = "${address.doorNo}" maxlength="15" 
                 placeholder="Eg:12/122A" required /></td>
            </tr>
            <tr>
                <td>Street</td>
                <td><input type="text" name="street" value = "${address.street}" maxlength="25"
                 pattern="[A-Za-z]*" placeholder="Eg:aaaaaa" required /></td>
            </tr>
            <tr>
                <td>Town</td>
                <td><input type="text" name="town" value = "${address.town}" maxlength="25"
                 pattern="[A-Za-z]*" placeholder="Eg:aaaaaa" required /></td>
            </tr>
            <tr>
                <td>District</td>
                <td><input type="text" name="district" value = "${address.district}" maxlength="25"
                 pattern="[A-Za-z]*" placeholder="Eg:aaaaaa" required /></td>
            </tr>
            <tr>
                <td>State</td>
                <td><input type="text" name="state" value = "${address.state}" maxlength="25"
                 pattern="[A-Za-z]*" placeholder="Eg:aaaaaa" required /></td>
            </tr>
        </c:forEach>    
    </table>
    <td><input type="hidden" name="id" value = "${employee.id}" /></td>
    <input type="hidden" name="operation" value="update"> 
    <center> <input value="Update" type="submit" class="button">
        <input type="reset"class="button"> 
    </center>
</form>
</body>
</html>
<%@ include file="footer.jsp"%>
